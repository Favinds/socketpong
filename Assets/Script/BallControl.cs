﻿ using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using SWNetwork;

public class BallControl : MonoBehaviour
{
    NetworkID networkID;
    public int scoreP1;
    public int scoreP2;
    public int force;
    GameObject scoreP1UI;
    GameObject scoreP2UI;

    // Start is called before the first frame update
    void Start()
    {
        networkID = GetComponent<NetworkID>();

        GetComponent<Rigidbody2D>().AddForce(new Vector2(0, 2).normalized * force);
        scoreP1 = 0;
        scoreP2 = 0;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnCollisionEnter2D(Collision2D coll)
    {
        if(networkID.IsMine)
        {
            if(coll.gameObject.name.Contains("p1"))
            {
                Vector2 dir = new Vector2(GetComponent<Rigidbody2D>().velocity.x, GetComponent<Rigidbody2D>().velocity.y).normalized;
                GetComponent<Rigidbody2D>().AddForce(dir*force);
            }

            else if(coll.gameObject.name == "batas_bawah")
            {
                scoreP2 += 1;
                ResetBall();
                if(scoreP2 == 10)
                {
                    // gameover condition
                }
                GetComponent<Rigidbody2D>().AddForce(new Vector2(0, -10).normalized * force);
            }

            else if(coll.gameObject.name == "batas_atas")
            {
                scoreP1 += 1;
                ResetBall();
                if(scoreP1 == 10)
                {
                    // gameover condition
                }
                GetComponent<Rigidbody2D>().AddForce(new Vector2(0, 10).normalized * force);
            }
        }
    }

    void ResetBall()
    {
        transform.localPosition = new Vector2(0,0);
        GetComponent<Rigidbody2D>().velocity = new Vector2(0,0);
        if(GameObject.FindGameObjectsWithTag("Player").Length == 1)
        {
            if(NetworkClient.Instance != null)
            {
                if(NetworkClient.Instance.IsHost)
                {
                    networkID.Destroy();
                }
                else
                {
                    Destroy(gameObject);
                }
            }
        }
    }

    void OnChangeScoreP1(int score)
    {
        if(scoreP1UI != null)
        {
            scoreP1UI.GetComponent<Text>().text = ""+score;
        }
        else
        {
            scoreP1UI = GameObject.Find("Score P1");
        }
    }

    void OnChangeScoreP2(int score)
    {
        if(scoreP2UI != null)
        {
            scoreP2UI.GetComponent<Text>().text = ""+score;
        }
        else
        {
            scoreP2UI = GameObject.Find("Score P2");
        }
    }
}

