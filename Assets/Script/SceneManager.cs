﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SWNetwork;

public class SceneManager : MonoBehaviour
{
    // public GameObject panel;
    // RoomPropertyAgent roomPropertyAgent;

    void Start(){
        // roomPropertyAgent = GetComponent<RoomPropertyAgent>();
    }

   public void OnSpawnerReady(bool alreadySetup, SceneSpawner sceneSpawener)
   {
       if(NetworkClient.Instance.IsHost)
       {
           sceneSpawener.SpawnForPlayer(0,1);
       }
       else
       {
           sceneSpawener.SpawnForPlayer(0, 0);
       }

       sceneSpawener.PlayerFinishedSceneSetup();
   }
}