﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SWNetwork;

public class move : MonoBehaviour
{
    NetworkID networkID;
    public Sprite player;
    float dirX;
    float speed = 15.0f;
    Rigidbody2D rb;
    // RemoteEventAgent remoteEventAgent;

    // Start is called before the first frame update
    void Start()
    {
        networkID = GetComponent<NetworkID>();
        rb = GetComponent<Rigidbody2D>();
        // remoteEventAgent = GetComponent<RemoteEventAgent>();
    }

    // Update is called once per frame
    void Update()
    {
        if(networkID.IsMine){
            // return;
            dirX = Input.acceleration.x * speed;
            transform.position = new Vector2(Mathf.Clamp(transform.position.x, -2.0f, 2.0f), transform.position.y);
            Debug.Log("bergerak");
        }
    }

    void FixedUpdate(){
        rb.velocity = new Vector2(dirX, 0f);
    }
}
